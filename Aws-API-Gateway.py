import json
import boto3
from botocore.exceptions import ClientError

def api_gateway(event, context):

    #1. Parse query string parameter
    databaseId = event['queryStringParameters']['databaseId']

    dynamodb = boto3.client('dynamodb')
    try:
        response = dynamodb.get_item(TableName='Twitter-Fingers', Key={'UserId':{'S': str(databaseId)}})
    except ClientError as e:
        response = "Data doesn't exist"

    #2. Construct the body of the response object
    databaseResponse = {}
    databaseResponse['Json Data'] = response

    #3. Construct http response object
    responseObject = {}
    responseObject['statusCode'] = 200
    responseObject['headers'] = {}
    responseObject['headers']['Content-Type'] = 'application/json'
    responseObject['body'] = json.dumps(databaseResponse)
    
    #4. Return the response object
    return responseObject
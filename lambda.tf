
locals {
  lambda_zip_location   = "outputs/Reko-DB-SNS-Script.zip"
  lambda_zip_location_2 = "outputs/Aws-API-Gateway.zip"
}

data "archive_file" "Reko-DB-SNS-Script" {
  type        = "zip"
  source_file = "Reko-DB-SNS-Script.py"
  output_path = local.lambda_zip_location
}

data "archive_file" "Aws-API-Gateway" {
  type        = "zip"
  source_file = "Aws-API-Gateway.py"
  output_path = local.lambda_zip_location_2
}

resource "aws_lambda_function" "my_lambda_function" {
  filename         = local.lambda_zip_location
  layers           = ["arn:aws:lambda:eu-west-1:770693421928:layer:Klayers-python38-pandas:48"]
  function_name    = "Reko-DB-SNS-Script"
  role             = aws_iam_role.lambda_iam_role.arn
  handler          = "Reko-DB-SNS-Script.lamdba_handler"
  source_code_hash = filebase64sha256(local.lambda_zip_location)
  runtime          = "python3.8"
  timeout          = 10

  tags = {
    Name = "brian-lambda-s3-reko-db-script"
  }
}

resource "aws_lambda_function" "my_lambda_function_api_gateway" {
  filename         = local.lambda_zip_location_2
  function_name    = "Aws-API-Gateway"
  role             = aws_iam_role.lambda_iam_role.arn
  handler          = "Aws-API-Gateway.api_gateway"
  source_code_hash = filebase64sha256(local.lambda_zip_location_2)
  runtime          = "python3.8"
  timeout          = 10

  tags = {
    Name = "brian-lambda-api-gateway-script"
  }
}

resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.my_lambda_function.arn
  principal     = "s3.amazonaws.com"
  source_arn    = aws_s3_bucket.prod-bucket.arn
}


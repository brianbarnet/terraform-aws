import json
import boto3
import uuid
from botocore.exceptions import ClientError
import pandas as pd

def lamdba_handler(event, context):

    s3_client = boto3.client('s3')
    bucket_name = 'twitter-fingers-bucket'

    #Get the latest image
    for key in s3_client.list_objects(Bucket=bucket_name)['Contents']:
        photo_name = key['Key']

    #Detect text
    client_image_rekognition=boto3.client('rekognition')

    response = client_image_rekognition.detect_text(Image={'S3Object':{'Bucket':bucket_name,'Name':photo_name}})
    
    content = []
    detected_text = response['TextDetections']
    for text in detected_text:
        content.append(text['DetectedText'])

    #Database processing
    dynamodb = boto3.client('dynamodb')
    userId = str(uuid.uuid4().hex)

    dynamodb.put_item(TableName='Twitter-Fingers', Item={'UserId':{'S': userId},'Data':{'S': json.dumps(content)}})

    #Getting AWS API Link
    try:
        gateway = boto3.client('apigatewayv2')
        response = gateway.get_apis()
        input_dict = json.dumps(response, indent=4, sort_keys=True, default=str)
        resp = json.loads(input_dict)
        df = pd.DataFrame(resp['Items'])
        api = pd.DataFrame(df[df['Name'] == "v2-http-api"]['ApiEndpoint'])
        apiUrl = api.iat[0,0]
    except ClientError as e:
        apiUrl = "Sorry: Url not generated"

    #Client notification
    client = boto3.client('sns')
    response = client.publish (
      TargetArn = "arn:aws:sns:eu-west-1:296274010522:twitter-fingers",
      Subject = "Image upload to S3 bucket:" + bucket_name,
      Message = "Good day,\n" + "Your image: " + photo_name + " has been processed successfully.\n" + "Here is your json link:\n" + apiUrl + "?databaseId=" + userId,
    )
        

    return {
        'statusCode': 200
    }


 
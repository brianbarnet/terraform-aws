variable "s3BucketName" {
  description = "The name of the storage"
}

variable "s3BucketTagName" {
  description = "The name of the storage tag"
}

#S3 Bucket resource block
resource "aws_s3_bucket" "prod-bucket" {
  bucket = var.s3BucketName
  acl    = "private"

  tags = {
    Name = var.s3BucketTagName
  }
}

resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = aws_s3_bucket.prod-bucket.id

  lambda_function {
    lambda_function_arn = aws_lambda_function.my_lambda_function.arn
    events              = ["s3:ObjectCreated:*"]
  }

  depends_on = [aws_lambda_permission.allow_bucket]
}
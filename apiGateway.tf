variable "apiGatewayName" {
  description = "The name of the api gateway"
}

variable "apiProtocolRule" {
  description = "The protocol to be used by the API"
}

variable "apiGatewayTagName" {
  description = "The name of the api tag name"
}

#Lambda API functionality

resource "aws_apigatewayv2_api" "lambda-api" {
  name          = var.apiGatewayName
  protocol_type = var.apiProtocolRule
}

resource "aws_apigatewayv2_stage" "lambda-stage" {
  api_id      = aws_apigatewayv2_api.lambda-api.id
  name        = "$default"
  auto_deploy = true

  tags = {
    Name = var.apiGatewayTagName
  }
}

resource "aws_apigatewayv2_integration" "lambda-intergration" {
  api_id               = aws_apigatewayv2_api.lambda-api.id
  integration_type     = "AWS_PROXY"
  integration_method   = "POST"
  integration_uri      = aws_lambda_function.my_lambda_function_api_gateway.invoke_arn
  passthrough_behavior = "WHEN_NO_MATCH"
}

resource "aws_apigatewayv2_route" "lambda_route" {
  api_id    = aws_apigatewayv2_api.lambda-api.id
  route_key = "GET /{proxy+}"
  target    = "integrations/${aws_apigatewayv2_integration.lambda-intergration.id}"
}

resource "aws_lambda_permission" "api-gw" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.my_lambda_function_api_gateway.arn
  principal     = "apigateway.amazonaws.com"
  source_arn    = "${aws_apigatewayv2_api.lambda-api.execution_arn}/*/*/*"
}
# Terraform AWS File Upload
This is a image upload application where the user uploads the image to s3 bucket. The uploaded image will then be processed by AWS Rekognition Service to extract text. The text is then saved to database (**DynamoDB**) and an email with the link to the text is sent to the user using (**AWS SNS Service**). The link is made form (**AWS API Gateway**) version 2 which allows the user to view the text in the browser.

![alt text](https://www.linkpicture.com/q/MicrosoftTeams-image-1_7.png)

## Amazon Services
- S3 bucket
- SNS (Amazon Simple Notification Service)
- DynamoDB
- AWS API Gateway
- Lambda 
- Amazon Rekognition

## Browser Support
- Google Chrome
- Microsoft Edge
- Mozilla Firefox
- Apple Safari

## Setup

For getting started with development, please follow the next steps.

### Prerequisites

Please make sure that the following tools are installed on your machine:

-   [Terrafrom](https://www.terraform.io/downloads) 
-   [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html) 

### Usage

- Clone the project with `git clone`
- Input your credentials in `terraform.tvars` file
- Run `terraform plan`
- Run `terraform apply`
- Open CMD and upload your image to S3 bucket `aws s3 cp YOUR_IMAGE_PATH s3://twitter-fingers-bucket/`
- When done, please run `terraform destroy`

## Demo Samples


#### 1) Email received when image has been uploaded

![alt text](https://www.linkpicture.com/q/Screenshot_1_186.png)


#### 2) URL Redirect to Json Data

![alt text](https://www.linkpicture.com/q/Screenshot_2_101.png)
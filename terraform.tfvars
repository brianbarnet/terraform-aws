# Credentials

accessKey = ""
secretKey = ""
region = ""

# Database Dynamo

databaseName = "Twitter-Fingers"
databaseHashKey = "UserId"
databaseNameTag = "brian-db"
databaseHashKeyType = "S"

# S3 Bucket

s3BucketName = "twitter-fingers-bucket"
s3BucketTagName = "brian-bucket"

# API Gateway
apiGatewayName = "v2-http-api"
apiGatewayTagName = "brian-api-gateway"
apiProtocolRule = "HTTP"
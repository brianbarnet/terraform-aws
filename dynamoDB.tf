variable "databaseName" {
  description = "The name of the database"
}

variable "databaseHashKey" {
   description = "The hash key of the database"
}

variable "databaseNameTag" {
   description = "The name tag of the database"
}

variable "databaseHashKeyType" {
  description = "The hash key type"
}

resource "aws_dynamodb_table" "basic-dynamodb-table" {
  name           = var.databaseName
  billing_mode   = "PROVISIONED"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = var.databaseHashKey

  attribute {
    name = var.databaseHashKey
    type = var.databaseHashKeyType
  }

  tags = {
    Name = var.databaseNameTag
  }
}